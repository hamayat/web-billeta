<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'web-billeta');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J<H4-p0~==%w1OV_iYYQiAIM`x ugzC9tB<Cj;v4t*ET{Xjy(4$m`q]@kQ@W7&:>');
define('SECURE_AUTH_KEY',  'mkz?K=C`vhIAgZ<C&@/u<ZVbz>*)&ST6{<XRLe?1?60d:}jIjzbRp6[,/VQuT{$n');
define('LOGGED_IN_KEY',    'vLGQk}V;/^1-!/xf)LMqj%cRd#W!.LGEE:g4]B2U{Ti>E7&K^kCK;0< s_KrztDl');
define('NONCE_KEY',        ')*p8,#FnF08nR7AsQ$snDquyyHjp9>=w]-R*cDd1)<Ek3#Pk5uk2WwH-::oU?l7=');
define('AUTH_SALT',        'ONbGZ=D@<R&<C%nh$fwBQcShmcO=e;l{tU3eafL>~~EuOQ<5b,p{M: c/8y+/oc}');
define('SECURE_AUTH_SALT', 'gv}))t*&85Qj2*U*~H4ic]`)(V%9::_<UeksvSwl*&Y}{MwvugC-o0F0gnjOl<u2');
define('LOGGED_IN_SALT',   'XtKIZq[ohzLP=!O<_eUHCfxQ6_ft4kHPobC,d|c!s=D`p}C&48W]]%Q4/{Eo?%NO');
define('NONCE_SALT',       'w|L8FzzunGRP2^}_yn&>wdClZ`Ll8i7S2)SiKDUy 87m1>OK[-S^Iy.j]y#29|j:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
